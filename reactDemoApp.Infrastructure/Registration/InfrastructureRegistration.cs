﻿using Microsoft.Extensions.DependencyInjection;
using reactDemoApp.Infrastructure.Contracts;
using reactDemoApp.Infrastructure.Services;
using reactDemoApp.Repository.Registration;

namespace reactDemoApp.Infrastructure.Registration
{
    public static class InfrastructureRegistration
    {

        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddRepository();

            services.AddScoped<IPersonService, PersonService>();

            return services;
        }

        //public static async Task MigrateDb(IApplicationBuilder app)
        //{
        //    RepositoryRegistration.MigrateDb(app);
        //}
    }
}
