﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using reactDemoApp.Domain.Enities;
using reactDemoApp.Domain.Models;
using reactDemoApp.Infrastructure.Contracts;
using reactDemoApp.Repository.Contracts;

namespace reactDemoApp.Infrastructure.Services  
{  
    public class PersonService :IPersonService 
    {
        private readonly IPersonRepository _personRepository;
        private readonly ICountryRepository _countryRepository;

        public PersonService(IPersonRepository personRepository, ICountryRepository countryRepository )
        {
            _countryRepository = countryRepository;
            _personRepository = personRepository;
        }
  
        public async Task<List<PersonModel>> GetAllPerson()  
        {  
            try  
            {  
                return await _personRepository.AsQueryable().Select(p => new PersonModel(p)).ToListAsync();  
            }  
            catch(Exception exception)  
            {  
                throw;  
            }  
        }  
  
        //To Add new employee record     
        public  async Task<int> AddPerson(PersonModel person)  
        {  
            try  
            {  
                _personRepository.Add(new PersonEntity(person));  
                await _personRepository.SaveChangesAsync();  
                return 1;  
            }  
            catch  
            {  
                throw;  
            }  
        }  
  
        //To Update the records of a particluar employee    
        public  async Task<int> UpdatePerson(PersonModel model)  
        {  
            try
            {
                var person = await _personRepository.FindBy(p=> p.Id == model.Id).FirstOrDefaultAsync();
                person.Name = model.Name;
                person.Country = model.Country;
                person.LastName = model.LastName;
                _personRepository.Update(person);
                await _personRepository.SaveChangesAsync();
  
                return 1;  
            }  
            catch(Exception ex)  
            {  
                throw;  
            }  
        }  
  
        //Get the details of a particular employee    
        public async Task<PersonModel> GetPersonData(int id)  
        {  
            try  
            {  
                var person = await _personRepository.FindBy(p =>p.Id == id).FirstOrDefaultAsync();
                var dto = new PersonModel()
                {
                    Id = person.Id,
                    LastName = person.LastName,
                    Country = person.Country,
                    Name = person.Name
                };
                return dto;  
            }  
            catch  
            {  
                throw;  
            }  
        }  
  
        //To Delete the record of a particular employee    
        public async Task<int> DeletePerson(int id)  
        {  
            try  
            {  
                PersonEntity emp = _personRepository.FindBy(p =>p.Id == id).FirstOrDefault();  
                _personRepository.Delete(emp);
                await _personRepository.SaveChangesAsync();  
                return 1;  
            }  
            catch  
            {  
                throw;  
            }  
        }  
  
        //To Get the list of Cities    
        public async Task<List<CountryEntity>> GetCountries()
        {

            var countries = await _countryRepository.AsQueryable().ToListAsync();
            return countries;
        }  
  
    }  
}  