﻿using System.Collections.Generic;
using System.Threading.Tasks;
using reactDemoApp.Domain.Enities;
using reactDemoApp.Domain.Models;

namespace reactDemoApp.Infrastructure.Contracts
{
    public interface IPersonService
    {
         Task<List<PersonModel>> GetAllPerson();
        Task<int> AddPerson(PersonModel person);
        Task<int> UpdatePerson(PersonModel model);
        Task<PersonModel> GetPersonData(int id);
        Task<int> DeletePerson(int id);
        Task<List<CountryEntity>> GetCountries();

    }
}
