﻿using System;
using reactDemoApp.Domain.Contracts;

namespace reactDemoApp.Domain.Enities
{
    public abstract class BaseEntity : IEntity<Int32>
    {
        public virtual Int32 Id { get; set; }
    }
}
