﻿using System;
using reactDemoApp.Domain.Models;

namespace reactDemoApp.Domain.Enities
{
    public class PersonEntity:BaseEntity
    {

        public PersonEntity()
        {
            
        }
        public PersonEntity(PersonModel person)
        {
            this.Name = person.Name;
            this.Country = person.Country;
            this.LastName = person.LastName;
        }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
    }
}
