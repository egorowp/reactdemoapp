﻿using System;
using System.Collections.Generic;
using System.Text;
using reactDemoApp.Domain.Enities;

namespace reactDemoApp.Domain.Models
{
    public class PersonModel
    {
        public PersonModel()
        {
            
        }
        public PersonModel(PersonEntity person)
        {
            Id = person.Id;
            LastName = person.LastName;
            Country = person.Country;
            Name = person.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
    }
}
