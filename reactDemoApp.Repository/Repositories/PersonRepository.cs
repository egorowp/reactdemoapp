﻿using System;
using System.Collections.Generic;
using System.Text;
using reactDemoApp.Domain.Contracts;
using reactDemoApp.Domain.Enities;
using reactDemoApp.Repository.Contracts;

namespace reactDemoApp.Repository.Repositories
{
    class PersonRepository:GenericRepository<PersonEntity>, IPersonRepository
    {
        public PersonRepository(IContext context) : base(context)
        {
        }
    }
}
