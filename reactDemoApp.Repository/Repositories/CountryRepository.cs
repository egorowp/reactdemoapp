﻿using System;
using System.Collections.Generic;
using System.Text;
using reactDemoApp.Domain.Contracts;
using reactDemoApp.Domain.Enities;
using reactDemoApp.Repository.Contracts;

namespace reactDemoApp.Repository.Repositories
{
    class CountryReposotory:GenericRepository<CountryEntity>, ICountryRepository
    {
        public CountryReposotory(IContext context) : base(context)
        {
        }
    }
}
