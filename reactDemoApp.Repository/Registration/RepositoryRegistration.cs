﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using reactDemoApp.Domain.Contracts;
using reactDemoApp.Repository.Contracts;
using reactDemoApp.Repository.EntityFramework;
using reactDemoApp.Repository.Repositories;

namespace reactDemoApp.Repository.Registration
{
    public static class RepositoryRegistration
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddDbContext<EfContext>();

            services.AddSingleton<IContext, EfContext>();

            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<ICountryRepository, CountryReposotory>();

            return services;

        }
        
        public static void MigrateDb(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<EfContext>();
                context.Database.Migrate();

            }
        }
    }
}
