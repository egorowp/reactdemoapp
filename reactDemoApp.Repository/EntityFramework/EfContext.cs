﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using reactDemoApp.Domain.Contracts;
using reactDemoApp.Domain.Enities;

namespace reactDemoApp.Repository.EntityFramework
{
    public partial class EfContext : DbContext, IContext
    {
        private readonly IConfiguration _configuration;

        public EfContext()
        {
        }

        public EfContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public virtual DbSet<CountryEntity> Countries { get; set; }
        public virtual DbSet<PersonEntity> Person { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = _configuration.GetConnectionString("DatabaseConnection");

            optionsBuilder.UseSqlServer(connectionString);

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CountryEntity>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("CountryID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PersonEntity>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("PersonID");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(true);
        }
    }
}
