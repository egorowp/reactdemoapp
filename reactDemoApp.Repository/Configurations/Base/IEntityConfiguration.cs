 

using Microsoft.EntityFrameworkCore;

namespace IonicNetCoreDemo.Repository.EntityFaramework.Configurations.Base
{
    public interface IEntityConfiguration
    {
        void AddConfiguration(ModelBuilder builder);
    }
}
