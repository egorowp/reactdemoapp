using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using reactDemoApp.Domain.Enities;

namespace IonicNetCoreDemo.Repository.EntityFaramework.Configurations.Base
{
    public abstract class ConfigurationBase<T> : IEntityTypeConfiguration<T>, IEntityConfiguration where T : BaseEntity
    {
        public void AddConfiguration(ModelBuilder builder)
        {
            builder.ApplyConfiguration(this);
        }
         
        protected String GetEntityName()
        {
            return typeof(T).Name.Replace("Entity", "");
        }

        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.ToTable(GetEntityName());

            builder.HasKey(x => x.Id);
            builder.Property(s => s.Id).HasColumnName($"{GetEntityName()}Id");
        }
    }
}
