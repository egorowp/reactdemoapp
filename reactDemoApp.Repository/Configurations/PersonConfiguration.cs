﻿using System;
using System.Collections.Generic;
using System.Text;
using IonicNetCoreDemo.Repository.EntityFaramework.Configurations.Base;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using reactDemoApp.Domain.Enities;

namespace reactDemoApp.Repository.Configurations
{
    class PersonConfiguration : ConfigurationBase<PersonEntity>
    {
        public override void Configure(EntityTypeBuilder<PersonEntity> builder)
        {
            base.Configure(builder);

            builder.Property(x => x.Name).IsRequired();
        }
    }
}
