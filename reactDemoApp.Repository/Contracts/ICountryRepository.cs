﻿using reactDemoApp.Domain.Enities;

namespace reactDemoApp.Repository.Contracts
{
    public interface ICountryRepository:IGenericRepository<CountryEntity>
    {
    }
}