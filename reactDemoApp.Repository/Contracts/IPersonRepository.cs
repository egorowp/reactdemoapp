﻿using System;
using System.Collections.Generic;
using System.Text;
using reactDemoApp.Domain.Enities;

namespace reactDemoApp.Repository.Contracts
{
    public interface IPersonRepository:IGenericRepository<PersonEntity>
    {
    }
}
