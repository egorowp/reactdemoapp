﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using reactDemoApp.Domain.Enities;
using reactDemoApp.Domain.Models;
using reactDemoApp.Infrastructure.Contracts;

namespace reactDemoApp.Controllers  
{  
    [Route("api/[controller]")]  
    public class PersonController : Controller  
    {
        private readonly IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }
        [HttpGet("[action]")]  
        public async Task<List<PersonModel>> Index()  
        {  
            return await _personService.GetAllPerson();  
        }  
  
        [HttpPost("[action]")]  
        public async Task<int> Create(PersonModel person)  
        {  
            return await _personService.AddPerson(person);  
        }  
  
        [HttpGet("[action]")]  
        public async Task<PersonModel> Details(int id)  
        {  
            return await _personService.GetPersonData(id);  
        }  
  
        [HttpPut("[action]")]  
        public async Task<int> Edit(PersonModel person)  
        {  
            return await _personService.UpdatePerson(person);  
        }  
  
        [HttpDelete("[action]")]  
        public async Task<int> Delete(int id)  
        {  
            return await _personService.DeletePerson(id);  
        }  
  
        [HttpGet("[action]")]  
        public async Task<List<CountryEntity>> GetCountryList()  
        {  
            return await _personService.GetCountries();  
        }  
    }  
}  