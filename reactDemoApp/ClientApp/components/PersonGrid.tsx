﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { Services } from "../generated-services";

interface PersonGridDataState {
    personList: any[];
    loading: boolean;
}

export class PersonGrid extends React.Component<RouteComponentProps<{}>, PersonGridDataState> {
    client = new Services.Client();
    state = { personList: [], loading: true };
    constructor(props: any) {
        super(props);
        this.state = { personList: [], loading: true };
        this.client.apiPersonIndexGet()
            .then(data => {
                this.setState({ personList: data, loading: false });
            });

        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);

    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderPersonTable(this.state.personList);

        return <div>
            <h1>Persons</h1>
            <p>
                <Link to="/addperson">Create New</Link>
                <br></br>
                <Link to="/datepicker">DatePicker</Link>
                <br></br>
                <Link to="/userform">UserForm</Link>
                <br></br>
                <Link to="/dynamic">Dynamic Form</Link>
            </p>
            {contents}
        </div>;
    }

    private handleDelete(id: number) {
        if (!confirm("Do you want to delete person with Id: " + id))
            return;
        else {
            this.client.apiPersonDeleteDelete(id)
                .then(data => {
                    this.setState((prevState: any, props: any) => {
                        return {
                            personList: this.state.personList.filter((rec: any) => {
                                return (rec.id !== id);
                            })
                        }
                    });
                });
        }
    }

    private handleEdit(id: number) {
        this.props.history.push("/Person/Edit/" + id);
    }

    private renderPersonTable(list: any[]) {
        return <table className='table'>
            <thead>
                <tr>
                    <th></th>
                    <th>PersonId</th>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Country</th>
                </tr>
            </thead>
            <tbody>
                {list.map(p =>
                    <tr key={p.id}>
                        <td></td>
                        <td>{p.id}</td>
                        <td>{p.name}</td>
                        <td>{p.lastName}</td>
                        <td>{p.country}</td>
                        <td>
                            <a onClick={() => this.handleEdit(p.id)}>Edit</a>  |
                            <a onClick={() => this.handleDelete(p.id)}>Delete</a>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}
