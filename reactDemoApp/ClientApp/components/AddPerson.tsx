﻿import * as React from 'react';  
import { RouteComponentProps } from 'react-router';  
import {Services}  from "../generated-services";  


interface AddPersonDataState {  
    title: string;  
    loading: boolean;  
    countryList: Array<any>;  
    personData: any;  
}  
  
export class AddPerson extends React.Component<RouteComponentProps<{}>, AddPersonDataState> {  
    state: { title: string; loading: boolean; countryList: never[]; personData: any; };
    client = new Services.Client();

    constructor(props: any) {  
        super(props);  
  
        this.state = {
            title: "", loading: true, countryList: [], personData: {}
        };

        this.client.apiPersonGetCountryListGet().then(data => {
            this.setState(() => { return { countryList: data } });
        });

        var personid = props.match.params["personid"];  
  
        if (personid > 0) {  
            this.client.apiPersonDetailsGet(personid)
                .then(data => {  
                    this.setState(() => { return{ title: "Edit", loading: false, personData: data } });  
                });  
        }  
  
        else {  
            this.state = {
                title: "Create", loading: false, countryList: [], personData: {} };  
        }  
  
        this.handleSave = this.handleSave.bind(this);  
        this.handleCancel = this.handleCancel.bind(this);  
    }  
    public render() {  
        let contents = this.state.loading  
            ? <p><em>Loading...</em></p>  
            : this.renderCreateForm(this.state.countryList);  
  
        return <div>  
            <h1>{this.state.title}</h1>  
            <h3>Person</h3>  
            <hr />  
            {contents}  
        </div>;  
    }  
  
    private handleSave(event: any) {  
        event.preventDefault();  
  
        if (this.state.personData.id) {
            this.client.apiPersonEditPut(event.target.id.value, event.target.name.value,event.target.lastName.value,event.target.country.value)
            .then((responseJson) => {
                    this.props.history.push("/getperson");
                });
        }  
  
        else {
            this.client.apiPersonCreatePost(event.target.id.value, event.target.name.value,event.target.lastName.value,event.target.country.value)
                .then((responseJson) => {
                this.props.history.push("/getperson");
            });
        }  
    }  
  
    private handleCancel(e: any) {  
        e.preventDefault();  
        this.props.history.push("/getperson");  
    }  
  
    private renderCreateForm(countryList: Array<any>) {  
        return (  
            <form onSubmit={this.handleSave} >  
                <div className="form-group row" >  
                    <input type="hidden" name="id" value={this.state.personData.id} />  
                </div>  
                <div className="form-group row" >  
                    <label className=" control-label col-md-12" htmlFor="name">Name</label>  
                    <div className="col-md-4">  
                        <input className="form-control" type="text" name="name" defaultValue={this.state.personData.name} required />  
                    </div>  
                </div >  
                <div className="form-group row">  
                    <label className="control-label col-md-12" htmlFor="lastName">Last Name</label>  
                    <div className="col-md-4">  
                        <input className="form-control" type="text" name="lastName" defaultValue={this.state.personData.lastName} required />  
                    </div>  

                </div >  
                <div className="form-group row">  
                    <label className="control-label col-md-12" htmlFor="country">Country</label>  
                    <div className="col-md-4">  
                        <select className="form-control" data-val="true" name="country" defaultValue={this.state.personData.country} required>  
                            <option value="">-- Select Country --</option>  
                            {countryList.map(country =>  
                                <option key={country.countryId} value={country.name}>{country.name}</option>  
                            )}  
                        </select>  
                    </div>  
                </div >  
                <div className="form-group">  
                    <button type="submit" className="btn btn-default">Save</button>  
                    <button className="btn" onClick={this.handleCancel}>Cancel</button>  
                </div >  
            </form >  
        );  
    }  
}