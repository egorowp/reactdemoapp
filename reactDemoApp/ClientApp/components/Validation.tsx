﻿import * as React from 'react';  
import { RouteComponentProps } from 'react-router';  

export class UserForm extends React.Component<RouteComponentProps<{}>, {  }>  {
    constructor(props) {
        super(props);
        var name = props.name;
        var nameIsValid = this.validateName(name);
        var age = props.age;
        var ageIsValid = this.validateAge(age);
        this.state = { name: name, age: age, nameValid: nameIsValid, ageValid: ageIsValid };

        this.onNameChange = this.onNameChange.bind(this);
        this.onAgeChange = this.onAgeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    validateAge(age) {
        return age && age >= 0;
    }
    validateName(name) {
        return name && name.length > 2;
    }
    onAgeChange(e) {
        var val = e.target.value;
        var valid = this.validateAge(val);
        this.setState({ age: val, ageValid: valid });
    }
    onNameChange(e) {
        var val = e.target.value;
        console.log(val);
        var valid = this.validateName(val);
        this.setState({ name: val, nameValid: valid });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.nameValid && this.state.ageValid) {
            alert("Name: " + this.state.name + " Age: " + this.state.age);
        }
    }
    state = {
        name: "",
        age: 10,
        nameValid: true,
        ageValid: true
    };
    render() {
        var nameColor = this.state.nameValid ? "green" : "red";
        var ageColor = this.state.ageValid ? "green" : "red";

        return (
            <form onSubmit={this.handleSubmit}>
                <p>
                    <label>Name:</label><br />
                    <input type="text" value={this.state.name}
                        onChange={this.onNameChange} style={{ borderColor: nameColor }} />
                </p>
                <p>
                    <label>Age:</label><br />
                    <input type="number" value={this.state.age}
                        onChange={this.onAgeChange} style={{ borderColor: ageColor }} />
                </p>
                <input type="submit" value="Send" />
            </form>
        );
    }
}
