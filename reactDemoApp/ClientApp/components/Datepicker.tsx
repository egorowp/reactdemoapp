﻿import * as React from 'react';  
import { RouteComponentProps } from 'react-router';  
import Datepicker from "react-datepicker";
import * as moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
  
export class DatepickerTest  extends React.Component<RouteComponentProps<{}>, {  }> {  
    state = {
            startDate: moment()
        };   
    constructor (props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(date) {
        this.setState({
            startDate: date
        });
    }
 
    render() {
        return <Datepicker
                   selected={this.state.startDate}
                   onChange={this.handleChange}
               />;
    }  
  
    
}