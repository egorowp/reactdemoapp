import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { PersonGrid } from "./components/PersonGrid";
import { AddPerson } from "./components/AddPerson";
import { DatepickerTest } from "./components/Datepicker";
import { UserForm } from "./components/Validation";
import { SimpleForm } from "./components/DynamicForm";

export const routes = <Layout>
    <Route exact path='/' component={PersonGrid} />
    <Route path='/getperson' component={PersonGrid} />
    <Route path='/addperson' component={AddPerson} />
    <Route path='/person/edit/:personid' component={AddPerson} />
    <Route path='/datepicker' component={DatepickerTest} />
    <Route path='/userform' component={UserForm} />
    <Route path='/dynamic' component={SimpleForm} />
</Layout>;
